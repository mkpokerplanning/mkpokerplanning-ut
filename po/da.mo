��    $      <  5   \      0  �   1     �     �  /   �     )     ;     S     \  	   o     y     �     �  	   �     �  
   �     �     �     �  w  �     ^     d     t     �     �     �     �  	   �     �     �     �     �               $     -  ?  C  �   �     )	     7	  .   L	     {	     �	     �	     �	     �	     �	  	   �	     �	     �	     �	     
     
     !
     6
  g  G
     �     �     �     �     �            	   #     -     A  	   Q     [     s          �     �                      $      #                      
                      "                                  !                                                             	             

Distributed under GPLv3 license.
Source code available on Gitlab
https://gitlab.com/mkpokerplanning/mkpokerplanning-ut
©2013-2014 mecadu, the doubting mechanism. 

Version 2.0 1,2,3,short text,... <b>Play poker planning with your scrum team</b> About this app... About this card back... Author:  Choose your card ! Deck back Deck values Family:  Here is my bet... License:  Name:  Play sound Settings Shake to reveal Stallman drawing The reason to use Planning poker is to avoid the influence of the other participants. If a number is spoken, it can sound like a suggestion and influence the other participants' sizing. Planning poker should force people to think independently and propose their numbers simultaneously. This is accomplished by requiring that all participants show their card at the same time. about about card back brave GNU world communism... custom series eiffel tower eiffel tower plan fibonacci fibonacci with coffee mkpokerplanning old style back project lifecycle (fr) random settings standard swing project (orig.) Project-Id-Version: 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-07-04 23:06+0200
PO-Revision-Date: 2015-07-04 23:12+0100
Last-Translator: Franck Routier <alcimecadu.org>
Language-Team: 
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.7.5
 

Distributed under GPLv3 license.
Source code available on Gitlab
https://gitlab.com/mkpokerplanning/mkpokerplanning-ut
©2013-2014 mecadu, den tvivlende mekanisme. 

Version 2.0 1,2,3,kort tekst,... <b>Spil poker planning med dit scrum team.</b> Om denne app... Om kort tilbage... Forfatter : Vælg dine kort ! Kort tilbage Kortværdier Familie : Her der mit bud... Licens : Navn : Afspille lyd Indstillinger... Ryst for at afslutte Stallman tegning Grunden til at bruge poker planning, er at undgå indflydelse fra de andre deltagere. Hvis et nummer bliver sagt højt, kan det lyde som et forslag og påvirke de andre deltageres estimat. Planning poker skal tvinge folk til at tænke uafhængigt og foreslå deres nummer samtidigt. Dette opnås ved at kræve at alle deltagere viser deres kort på samme tid. Om kort tilbage... Om kort tilbage... brave GNU world kommunismen... custom-serien eiffeltårnet eiffeltårnet plan fibonacci fibonacci med kaffe mkpokerplanning old style projekt livscyclus (fr) tilfældigt Indstillinger... standard swing projekt (orig.) 