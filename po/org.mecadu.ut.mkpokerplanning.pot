# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2015-07-04 23:34+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../app/components/CardBacksLibrary.qml:152
msgid "Tarot cards have been used to predict future for ages..."
msgstr ""

#: ../app/components/CardBacksLibrary.qml:153
msgid ""
"The tree swing cartoon has been around for several decades, and its origin "
"remains uncertain (see http://www.businessballs.com/treeswing.htm).\n"
"But it certainly depicts some common project management problems :-)"
msgstr ""

#: ../app/components/CardBacksLibrary.qml:154
msgid ""
"Project lifcycle: euphoria, concern, panics, seek the culprits, punish the "
"innocents, reward those that were not involved..."
msgstr ""

#: ../app/components/CardBacksLibrary.qml:155
msgid ""
"Richard Stallman launched the GNU project in 1983 and is the father of free "
"software. He created the FSF in 1985. He is also the author of Emacs and "
"many other software"
msgstr ""

#: ../app/components/CardBacksLibrary.qml:156
msgid ""
"In year 2000, Steve Ballmer of Microsoft, compared Linux to communism. It "
"seems it was an insult in his mouth :-)"
msgstr ""

#: ../app/components/CardBacksLibrary.qml:157
msgid ""
"The eiffel tower was a prototype, the first tower of more than 300 meters. "
"It was built on budget and on time..."
msgstr ""

#: ../app/ui/AboutBackPage.qml:33
msgid "About this card back..."
msgstr ""

#: ../app/ui/AboutBackPage.qml:51
msgid "Name: "
msgstr ""

#: ../app/ui/AboutBackPage.qml:62
msgid "Family: "
msgstr ""

#: ../app/ui/AboutBackPage.qml:74
msgid "License: "
msgstr ""

#: ../app/ui/AboutBackPage.qml:86
msgid "Author: "
msgstr ""

#: ../app/ui/AboutPage.qml:29
msgid "About this app..."
msgstr ""

#: ../app/ui/AboutPage.qml:49
msgid "<b>Play poker planning with your scrum team</b>"
msgstr ""

#: ../app/ui/AboutPage.qml:61
msgid ""
"The reason to use Planning poker is to avoid the influence of the other "
"participants. If a number is spoken, it can sound like a suggestion and "
"influence the other participants' sizing. Planning poker should force people "
"to think independently and propose their numbers simultaneously. This is "
"accomplished by requiring that all participants show their card at the same "
"time."
msgstr ""

#: ../app/ui/AboutPage.qml:69
msgid ""
"\n"
"\n"
"Version 2.0"
msgstr ""

#: ../app/ui/AboutPage.qml:76
msgid ""
"\n"
"\n"
"Distributed under GPLv3 license.\n"
"Source code available on Gitlab\n"
"https://gitlab.com/mkpokerplanning/mkpokerplanning-ut\n"
"©2013-2014 mecadu, the doubting mechanism."
msgstr ""

#: ../app/ui/CardPage.qml:37
msgid "Here is my bet..."
msgstr ""

#: ../app/ui/CardPage.qml:59
msgid "about card back"
msgstr ""

#: ../app/ui/MainPage.qml:32
msgid "Choose your card !"
msgstr ""

#: ../app/ui/MainPage.qml:37
msgid "settings"
msgstr ""

#: ../app/ui/MainPage.qml:44
msgid "about"
msgstr ""

#: ../app/ui/SettingsPage.qml:34
msgid "Settings"
msgstr ""

#: ../app/ui/SettingsPage.qml:60
msgid "Deck values"
msgstr ""

#: ../app/ui/SettingsPage.qml:61
msgid "standard"
msgstr ""

#: ../app/ui/SettingsPage.qml:62
msgid "fibonacci"
msgstr ""

#: ../app/ui/SettingsPage.qml:63
msgid "fibonacci with coffee"
msgstr ""

#: ../app/ui/SettingsPage.qml:64
msgid "custom series"
msgstr ""

#: ../app/ui/SettingsPage.qml:83
msgid "1,2,3,short text,..."
msgstr ""

#: ../app/ui/SettingsPage.qml:94
msgid "Deck back"
msgstr ""

#: ../app/ui/SettingsPage.qml:95
msgid "random"
msgstr ""

#: ../app/ui/SettingsPage.qml:96
msgid "old style back"
msgstr ""

#: ../app/ui/SettingsPage.qml:97
msgid "swing project (orig.)"
msgstr ""

#: ../app/ui/SettingsPage.qml:98
msgid "project lifecycle (fr)"
msgstr ""

#: ../app/ui/SettingsPage.qml:99
msgid "brave GNU world"
msgstr ""

#: ../app/ui/SettingsPage.qml:100
msgid "Stallman drawing"
msgstr ""

#: ../app/ui/SettingsPage.qml:101
msgid "communism..."
msgstr ""

#: ../app/ui/SettingsPage.qml:102
msgid "eiffel tower"
msgstr ""

#: ../app/ui/SettingsPage.qml:103
msgid "eiffel tower plan"
msgstr ""

#: ../app/ui/SettingsPage.qml:131
msgid "Shake to reveal"
msgstr ""

#: ../app/ui/SettingsPage.qml:151
msgid "Play sound"
msgstr ""

#: /home/franck/devel/qt-mobile/build-mkpokerplanning-ut-UbuntuSDK_for_armhf_GCC_ubuntu_sdk_15_04_vivid-Default/po/mkpokerplanning.desktop.in.h:1
msgid "mkpokerplanning"
msgstr ""
