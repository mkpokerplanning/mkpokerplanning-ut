��    *      l  ;   �      �  �   �     N     \  /   q     �     �     �     �  	   �     �     �       q     	   �     �  
   �  |   �  �   #     �     �     �  8   �  p   /  w  �  �   	     �	     �	     
     
     
     -
     :
  	   L
     V
     l
     |
     �
     �
     �
     �
     �
  ?  �
  �        �     �  8   �     6     C  	   c     m     �     �  
   �     �  �   �  
   N     Y     `  �   o  �     	   �     �     �  C      �   D  +  �  �        �          &     6     D     Z     f  	   }     �     �     �     �  
   �  	   �     �     �                       '         #                                                     
                            !      )   	                 $              %                   *   (   "   &                 

Distributed under GPLv3 license.
Source code available on Gitlab
https://gitlab.com/mkpokerplanning/mkpokerplanning-ut
©2013-2014 mecadu, the doubting mechanism. 

Version 2.0 1,2,3,short text,... <b>Play poker planning with your scrum team</b> About this app... About this card back... Author:  Choose your card ! Deck back Deck values Family:  Here is my bet... In year 2000, Steve Ballmer of Microsoft, compared Linux to communism. It seems it was an insult in his mouth :-) License:  Name:  Play sound Project lifcycle: euphoria, concern, panics, seek the culprits, punish the innocents, reward those that were not involved... Richard Stallman launched the GNU project in 1983 and is the father of free software. He created the FSF in 1985. He is also the author of Emacs and many other software Settings Shake to reveal Stallman drawing Tarot cards have been used to predict future for ages... The eiffel tower was a prototype, the first tower of more than 300 meters. It was built on budget and on time... The reason to use Planning poker is to avoid the influence of the other participants. If a number is spoken, it can sound like a suggestion and influence the other participants' sizing. Planning poker should force people to think independently and propose their numbers simultaneously. This is accomplished by requiring that all participants show their card at the same time. The tree swing cartoon has been around for several decades, and its origin remains uncertain (see http://www.businessballs.com/treeswing.htm).
But it certainly depicts some common project management problems :-) about about card back brave GNU world communism... custom series eiffel tower eiffel tower plan fibonacci fibonacci with coffee mkpokerplanning old style back project lifecycle (fr) random settings standard swing project (orig.) Project-Id-Version: 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-07-04 23:21+0200
PO-Revision-Date: 2015-07-04 23:28+0100
Last-Translator: Franck Routier <alcimecadu.org>
Language-Team: 
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.7.5
 
        Distribué sous licence GPLv3.
        Code source disponible sur Gitorious
        https://gitlab.com/mkpokerplanning/mkpokerplanning-ut
        ©2013-2014 mecadu, le mécanisme dubitatif. 

Version 2.0 1,2,3,texte court,... <b>Jouez au planning poker avec votre équipe scrum.</b> À propos... À propos de ce dos de carte... Auteur :  Choisissez votre carte ! Dos de cartes Valeurs du jeu Famille :  Mon évaluation... En l'an 2000, Steve Ballmar, de chez Microsoft, a comparé Linux au communisme. Il semble que dans sa bouche, il s'agissait d'une insulte... Licence :  Nom :  Jouer des sons Cycle de vie du projet : euphorie, inquiétude, panique, recherche des coupables, punition des innocents, promotion de ceux qui n'ont pas trempé dans le projet... Richard Stallman a lancé le projet GNU en 1983 et est le père du logiciel libre. Il a créé la FSF en 1985. Il est également l'auteur d'Emacs et de plusieurs autres logiciels majeurs Réglages Secouer pour montrer Dessin de Stallman Les Tarots ont servi à prédire l'avenir depuis très longtemps... La tour Eiffel était un prototype, la première tour de plus de 300 mètres. Elle a été réalisée dans le respect des budgets et des délais... La raison d'être du planning poker est d'éviter que les participants ne s'influencent mutuellement. Si une évaluation est annoncée, cela aura un effet sur l'évaluation des autres. Avec le planning poker, chacun dévoile sa carte en même temps, et est encouragé à réfléchir indépendamment. Le dessin humoristique sur le projet balançoire a plusieurs dizaines d'années, et ses origines demeurent incertaines (voir http://www.businessballs.com/treeswing.htm).
Mais il dépeint sûrement des problèmes de gestion de projet communs :-) À propos... À propos de ce dos de carte... brave GNU world communisme... série personnalisée tour Eiffel plan de la tour Eiffel fibonacci fibonacci avec le café mkpokerplanning ancien cycle de vie aléatoire Réglages standard Projet balançoire (orig.) 