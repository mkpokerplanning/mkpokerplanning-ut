/*
  Copyright (C) 2013 Franck Routier, Mecadu, the doubting mechanism
  Contact: Franck Routier <alci@mecadu.org>
  All rights reserved.

  This file is part of MkPokerPlanning.

  MkPokerPlanning is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  MkPokerPlanning is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with MkPokerPlanning.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.0
import Ubuntu.Components 1.2


Page {
    id: aboutPage

    title: i18n.tr("About this app...")

    Flickable {
        id: flickable
        anchors.fill: parent

        clip: true
        contentHeight: column.height

        Column {
            id: column

            width: parent.width
            spacing: units.gu(2)

            Label {
                width: parent.width
                wrapMode: Text.WordWrap

                horizontalAlignment: Text.Center
                text: i18n.tr("<b>Play poker planning with your scrum team</b>")
            }
            Label {
                anchors {
                            left: parent.left
                            right: parent.right
                            margins: units.gu(3)
                }
                wrapMode: Text.WordWrap
                fontSize: "medium"

                //horizontalAlignment: Text.Center
                text: i18n.tr("The reason to use Planning poker is to avoid the influence of the other participants. If a number is spoken, it can sound like a suggestion and influence the other participants' sizing. Planning poker should force people to think independently and propose their numbers simultaneously. This is accomplished by requiring that all participants show their card at the same time.")
            }

	   Label {
                width: parent.width
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.Center
                fontSize: "small"
                text: i18n.tr("\n\nVersion 2.0")
            }
            Label {
                width: parent.width
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.Center
                fontSize: "small"
                text: i18n.tr("\n\nDistributed under GPLv3 license.\nSource code available on Gitlab\nhttps://gitlab.com/mkpokerplanning/mkpokerplanning-ut\n©2013-2014 mecadu, the doubting mechanism.")
            }
        }
    }
}
