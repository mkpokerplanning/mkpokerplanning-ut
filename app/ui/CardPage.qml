/*
  Copyright (C) 2013 Franck Routier, Mecadu, the doubting mechanism
  Contact: Franck Routier <alci@mecadu.org>
  All rights reserved.

  This file is part of MkPokerPlanning.

  MkPokerPlanning is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  MkPokerPlanning is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with MkPokerPlanning.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.0
import QtMultimedia 5.4
import Ubuntu.Components 1.2
import "../components/"


Page {
    id: cardPage
    property string backImageSource
    property string value
    property string backname

    property AppSettings appSettings
    property CardBacksLibrary backsLibrary

    title: i18n.tr("Here is my bet...")

    ShakeSensor {
        id: shakeSensor
        active: card.state === "back" && Qt.application.active && appSettings.getShakeToReveal()

        onShaked: {
            if (card.state === "back") {
                card.state = "front"
            }
        }

    }

    Audio {
        id: playSound
        source: "../sounds/Frottement-mains-1s.wav"
    }

    head.actions: [
                Action {
                    iconName: "help"
                    text: i18n.tr("about card back")
                    onTriggered: {
                        pageStack.push(Qt.resolvedUrl("AboutBackPage.qml"),{"backname": backname, "backsLibrary": backsLibrary})
                    }
                }
            ]

    Flickable {

        //anchors.centerIn: parent
        //anchors.horizontalCenter: parent.horizontalCenter
        anchors.fill: parent
        //anchors.top: parent.top
        anchors.topMargin: units.gu(2)
        anchors.leftMargin: units.gu(5)

        Card {
            //anchors.centerIn: parent
            id: card
            backImageSource: cardPage.backImageSource
            value: cardPage.value
            fullSize: true
            state: "back"
        }
    }

    onVisibleChanged: {
       if (visible) {
           card.state = "back";
       }
   }

    Timer {
        interval: 5000
        running: card.state==="back" && Qt.application.active
        repeat: true
        onTriggered: {
            card.shake();
            if(appSettings.getPlaySound()) {
               playSound.play();
            }
        }
    }

}
