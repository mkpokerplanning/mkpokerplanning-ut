/*
  Copyright (C) 2013 Franck Routier, Mecadu, the doubting mechanism
  Contact: Franck Routier <alci@mecadu.org>
  All rights reserved.

  This file is part of MkPokerPlanning.

  MkPokerPlanning is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  MkPokerPlanning is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with MkPokerPlanning.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.0
import Ubuntu.Components 0.1
import Ubuntu.Components.ListItems 0.1
import "../components/"


Page {
    id: settingsPage

    property AppSettings appSettings
    property CardBacksLibrary backsLibrary

    title: i18n.tr("Settings")

    function getValuesIndex() {
        if (appSettings.getDeckValues() === "std")
            return 0
        else if (appSettings.getDeckValues() === "fibonacci")
            return 1
        else if (appSettings.getDeckValues() === "fibocafe")
            return 1
        else return 3
    }

    Flickable {
        anchors.fill: parent
        anchors.leftMargin: settingsPage.width/30
        anchors.rightMargin: settingsPage.width/30
        anchors.topMargin: 10

        Column {
            width: parent.width
            height: parent.height
            spacing: units.gu(3)

            OptionSelector {
                id: deckValues
                objectName: "deckValuesSelector"
                text: i18n.tr("Deck values")
                model: [i18n.tr("standard"),
                        i18n.tr("fibonacci"),
                        i18n.tr("fibonacci with coffee"),
                        i18n.tr("custom series")]
                selectedIndex: getValuesIndex()
                onSelectedIndexChanged: {
                    if(selectedIndex === 0)
                        appSettings.setDeckValues("std")
                    if(selectedIndex === 1)
                        appSettings.setDeckValues("fibonacci")
                    if(selectedIndex === 2)
                        appSettings.setDeckValues("fibocafe")
                    if(selectedIndex === 3)
                        appSettings.setDeckValues("custom")
                }
            }

            TextField {
               id: custsertext
               visible: (appSettings.getDeckValues() === "custom")
               width: parent.width
               //label: qsTr("Custom series")
               placeholderText: i18n.tr("1,2,3,short text,...")
               focus: false
               onAccepted: {
                   appSettings.setCustomSeries(text)
                   parent.focus = true;
               }
            }

            OptionSelector {
                id: backValue
                objectName: "deckBackSelector"
                text: i18n.tr("Deck back")
                model: [i18n.tr("random"),
                        i18n.tr("old style back"),
                        i18n.tr("swing project (orig.)"),
                        i18n.tr("project lifecycle (fr)"),
                        i18n.tr("brave GNU world"),
                        i18n.tr("Stallman drawing"),
                        i18n.tr("communism..."),
                        i18n.tr("eiffel tower"),
                        i18n.tr("eiffel tower plan")]
                selectedIndex: backsLibrary.getBackIndex(appSettings.getDeckBack())
                onSelectedIndexChanged: {
                    if(selectedIndex === 0)
                        appSettings.setDeckBack("random")
                    if(selectedIndex === 1)
                        appSettings.setDeckBack("old")
                    if(selectedIndex === 2)
                        appSettings.setDeckBack("swingorig")
                    if(selectedIndex === 3)
                        appSettings.setDeckBack("lifecycle-fr")
                    if(selectedIndex === 4)
                        appSettings.setDeckBack("brave-gnu-world")
                    if(selectedIndex === 5)
                        appSettings.setDeckBack("stallman")
                    if(selectedIndex === 6)
                        appSettings.setDeckBack("communism")
                    if(selectedIndex === 7)
                        appSettings.setDeckBack("eiffel")
                    if(selectedIndex === 8)
                        appSettings.setDeckBack("eiffel_plan")
                }
            }


            Standard {
                id: shakeSwitch

                text: i18n.tr("Shake to reveal")
                showDivider: false
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: units.gu(-2)
                }

                control: Switch {
                    objectName: "shakeCheckbox"
                    checked: appSettings.getShakeToReveal()
                    onClicked: {
                        appSettings.setShakeToReveal(checked)
                    }
                }
            }

            Standard {
                id: soundSwitch

                text: i18n.tr("Play sound")
                showDivider: false
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: units.gu(-2)
                }

                control: Switch {
                    objectName: "soundCheckbox"
                    checked: appSettings.getPlaySound()
                    onClicked: {
                        appSettings.setPlaySound(checked)
                    }
                }
            }
        }
    }
}
