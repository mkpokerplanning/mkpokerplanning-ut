/*
  Copyright (C) 2013 Franck Routier, Mecadu, the doubting mechanism
  Contact: Franck Routier <alci@mecadu.org>
  All rights reserved.

  This file is part of MkPokerPlanning.

  MkPokerPlanning is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  MkPokerPlanning is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with MkPokerPlanning.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.0
import Ubuntu.Components 1.2
import "../components"

Page {
    id: page

    property AppSettings settings: AppSettings{}
    property CardBacksLibrary backsLibrary: CardBacksLibrary{}

    title: i18n.tr("Choose your card !")

    head.actions: [
                Action {
                    iconName: "settings"
                    text: i18n.tr("settings")
                    onTriggered: {
                        pageStack.push(Qt.resolvedUrl("SettingsPage.qml"),{"appSettings": settings, "backsLibrary": backsLibrary})
                    }
                },
                Action {
                    iconName: "help"
                    text: i18n.tr("about")
                    onTriggered: {
                        pageStack.push(Qt.resolvedUrl("AboutPage.qml"))
                    }
                }
            ]

    Flickable {
        anchors.fill: parent
//        anchors.horizontalCenter: parent.horizontalCenter
        anchors.leftMargin: units.gu(2)
        anchors.topMargin: units.gu(1)

        GridView {
            id: gridView
            anchors.fill: parent
            cellHeight: page.height/4.5
            cellWidth: page.height/4.5/1.65

            model: CardDeckModel {
                id: deckModel
                series: settings.getDeckValues()
                backsource: backsLibrary.getCardBackFromName(settings.getDeckBack()).source
            }

            delegate: Card {
                id: myCard

                backImageSource: ico
                value: val
                cardname: bname
                height: gridView.cellHeight-units.gu(1)
                fullSize: false

                onClicked: {
                    pageStack.push(Qt.resolvedUrl("CardPage.qml"),
                                   {"appSettings": settings,
                                       "backImageSource": myCard.backImageSource,
                                       "backname": myCard.cardname,
                                       "value": myCard.value,
                                       "backsLibrary": backsLibrary
                                      })
                }
            }
        }
    }


    onActiveChanged: {
       if (active) {
           deckModel.backname = backsLibrary.getCardBackFromName(settings.getDeckBack()).settingsName
           deckModel.backsource = backsLibrary.getCardBackFromName(deckModel.backname).source
           deckModel.series = settings.getDeckValues()
           deckModel.customseries = settings.getCustomSeriesArray()
           deckModel.init()
       }
   }
}
