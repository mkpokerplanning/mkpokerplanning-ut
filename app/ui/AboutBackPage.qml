/*
  Copyright (C) 2013 Franck Routier, Mecadu, the doubting mechanism
  Contact: Franck Routier <alci@mecadu.org>
  All rights reserved.

  This file is part of MkPokerPlanning.

  MkPokerPlanning is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  MkPokerPlanning is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with MkPokerPlanning.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.0
import Ubuntu.Components 0.1
import "../components/"


Page {
    id: aboutBackPage

    property string backname
    property CardBacksLibrary backsLibrary

    title: i18n.tr("About this card back...")


    Column {
        id: column

        spacing: units.gu(1)
        anchors.fill: parent

        Label {
            anchors {
                        left: parent.left
                        right: parent.right
                        margins: units.gu(3)
            }
            wrapMode: Text.WordWrap
            fontSize: "medium"

            text: i18n.tr("Name: ") +backname
        }
        Label {
            anchors {
                        left: parent.left
                        right: parent.right
                        margins: units.gu(3)
            }
            wrapMode: Text.WordWrap
            fontSize: "medium"
            //horizontalAlignment: Text.Center
            text: i18n.tr("Family: ") + backsLibrary.getCardBackFromName(backname).family
        }
        Label {
            anchors {
                        left: parent.left
                        right: parent.right
                        margins: units.gu(3)
            }
            wrapMode: Text.WordWrap
            fontSize: "medium"

            //horizontalAlignment: Text.Center
            text: i18n.tr("License: ") + backsLibrary.getCardBackFromName(backname).license
        }
        Label {
            anchors {
                        left: parent.left
                        right: parent.right
                        margins: units.gu(3)
            }
            wrapMode: Text.WordWrap
            fontSize: "medium"

            //horizontalAlignment: Text.Center
            text: i18n.tr("Author: ") + backsLibrary.getCardBackFromName(backname).author
        }

        Label {
            anchors {
                        left: parent.left
                        right: parent.right
                        margins: units.gu(3)
            }
            width: parent.width
            wrapMode: Text.WordWrap
            fontSize: "medium"

            //horizontalAlignment: Text.Center
            text: i18n.tr(backsLibrary.getCardBackFromName(backname).about)
        }
    }
}
