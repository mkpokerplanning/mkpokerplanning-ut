/*
  Copyright (C) 2013 Franck Routier, Mecadu, the doubting mechanism
  Contact: Franck Routier <alci@mecadu.org>
  All rights reserved.

  This file is part of MkPokerPlanning.

  MkPokerPlanning is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  MkPokerPlanning is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with MkPokerPlanning.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
  This work is strongly indebted to QShake by Sebastiano Galazzo <sebastiano.galazzo@gmail.com>
*/
import QtQuick 2.0
import QtSensors 5.0

Accelerometer {

    signal shaked
    property real sensitivity: 30
    property real module: 0

    active: true
    dataRate: 20

    onReadingChanged: {
        var rx = reading.x;
        var ry = reading.y;
        var rz = reading.z;
        var cmodule = Math.sqrt(rx * rx + ry * ry + rz * rz);
        var cshake = Math.sqrt(Math.pow(module,2)+Math.pow(cmodule,2));

        if( cshake >= sensitivity ) {
            shaked();
        }

        module = cmodule;
    }
}
