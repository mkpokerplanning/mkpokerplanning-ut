/*
  Copyright (C) 2013 Franck Routier, Mecadu, the doubting mechanism
  Contact: Franck Routier <alci@mecadu.org>
  All rights reserved.

  This file is part of MkPokerPlanning.

  MkPokerPlanning is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  MkPokerPlanning is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with MkPokerPlanning.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.0

ListModel {

    id: cardDeckModel

    property string series: "fibonacci"
    property var customseries: ["0", "1"]
    property string backsource: "qrc:///backs/backold.png"
    property string backname

    function init() {
        var fibo = ["0", "1", "2", "3", "5", "8", "13", "21", "34", "55", "89"]
        var fibowithcafe = ["0","½", "1", "2", "3", "5", "8", "13", "21", "34", "55", "89", qsTr("Café")]
        var std = ["0","½","1","2","3","5","8","13","20","40","100","?",qsTr("Café")]
        var values

        if (series === "fibonacci") {
            values = fibo
        }
        else if (series === "fibocafe") {
            values = fibowithcafe
        }
        else if (series === "custom") {
            values = customseries
        }
        else {
            values = std
        }

        clear()

        var i=0
        for (i=0; i < values.length; i++) {
            append({"ico":backsource,"val":values[i],"bname":backname})
        }
    }

    Component.onCompleted: {
        init()
    }
}
