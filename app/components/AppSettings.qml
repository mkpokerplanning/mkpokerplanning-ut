/*
  Copyright (C) 2013 Franck Routier, Mecadu, the doubting mechanism
  Contact: Franck Routier <alci@mecadu.org>
  All rights reserved.

  This file is part of MkPokerPlanning.

  MkPokerPlanning is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  MkPokerPlanning is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with MkPokerPlanning.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.0
import QtQuick.LocalStorage 2.0

QtObject {

    property var db: null
    property string deckvalues: ""
    property string deckback: ""
    property string shakeToReveal: ""
    property string customseries: ""
    property string playSound: ""
    property var customseriesarray: customseries.split(",")

    function openDB() {
        if(db !== null) return;

        // db = LocalStorage.openDatabaseSync(identifier, version, description, estimated_size, callback(db))
        db = LocalStorage.openDatabaseSync("MkPokerPlanning", "1.0", "Mecadu Poker Planning", 1000);

        try {
            db.transaction(function(tx){
                tx.executeSql('CREATE TABLE IF NOT EXISTS settings(key TEXT UNIQUE, value TEXT)');
                var table  = tx.executeSql("SELECT * FROM settings");
                // Seed the table with default values
                if (table.rows.length === 0) {
                    tx.executeSql('INSERT INTO settings VALUES(?, ?)', ["deckvalues", "std"]);
                    tx.executeSql('INSERT INTO settings VALUES(?, ?)', ["deckback", "old"]);
                    tx.executeSql('INSERT INTO settings VALUES(?, ?)', ["shakeToReveal", "true"]);
                    tx.executeSql('INSERT INTO settings VALUES(?, ?)', ["playSound", "true"]);
                };
            });
        } catch (err) {
            console.log("Error creating table in database: " + err);
        };
    }


    function saveSetting(key, value) {
        openDB();
        db.transaction( function(tx){
            tx.executeSql('INSERT OR REPLACE INTO settings VALUES(?, ?)', [key, value]);
        });
    }

    function getSetting(key) {
        openDB();
        var res = "";
        db.transaction(function(tx) {
            var rs = tx.executeSql('SELECT value FROM settings WHERE key=?;', [key]);
            if (rs.rows.length > 0) {
                res = rs.rows.item(0).value;
            }
        });
        return res;
    }

    function getDeckValues() {
        if (deckvalues === "") {
            deckvalues = getSetting("deckvalues")
        }
        return deckvalues
    }

    function getDeckBack() {
        if (deckback === "") {
            deckback = getSetting("deckback")
        }
        return deckback
    }

    function getCustomSeries() {
        if (customseries === "") {
            customseries = getSetting("customseries")
        }
        return customseries
    }

    function getCustomSeriesArray() {
        if (customseries === "") {
            customseries = getSetting("customseries")
        }
        customseriesarray = customseries.split(",")
        return customseriesarray
    }

    function getShakeToReveal() {
        if (shakeToReveal === "") {
            shakeToReveal = getSetting("shakeToReveal")
        }
        if (shakeToReveal === "true")
            return true
        else
            return false
    }

    function getPlaySound() {
        if (playSound === "") {
            playSound = getSetting("playSound")
        }
        if (playSound === "true")
            return true
        else
            return false
    }

    function setDeckValues(val) {
        deckvalues = val
        saveSetting("deckvalues", val)
    }

    function setDeckBack(val) {
        deckback = val
        saveSetting("deckback", val)
    }

    function setCustomSeries(val) {
        customseries = val
        saveSetting("customseries", val)
    }


    function setShakeToReveal(val) {
        if (val)
            shakeToReveal = "true"
        else
            shakeToReveal = "false"
        saveSetting("shakeToReveal", shakeToReveal)
    }

    function setPlaySound(val) {
        if (val)
            playSound = "true"
        else
            playSound = "false"
        saveSetting("playSound", playSound)
    }

}
