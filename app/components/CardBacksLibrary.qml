/*
  Copyright (C) 2013 Franck Routier, Mecadu, the doubting mechanism
  Contact: Franck Routier <alci@mecadu.org>
  All rights reserved.

  This file is part of MkPokerPlanning.

  MkPokerPlanning is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  MkPokerPlanning is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with MkPokerPlanning.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.0
import Ubuntu.Components 1.2

ListModel {
    id: backsLibrary

    ListElement {
        settingsName: "random"
        source: "../graphics/backs/backold.png"
        description: "random"
        family: "all"
        license: "multiple"
        author: "multiple"
        about: "muliple"
    }

    ListElement {
        settingsName: "old"
        source: "../graphics/backs/backold.png"
        description: "old style back"
        family: "historical"
        license: "CC-BY-SA"
        author: "Pressburger"
        about: "Tarot cards have been used to predict future for ages..."
    }

    ListElement {
        settingsName: "swingorig"
        source: "../graphics/backs/swing-old.png"
        description: "swing project (orig.)"
        family: "humor"
        license: "Unknown"
        author: "S. Høgh"
        about: "The tree swing cartoon has been around for several decades, and its origin remains uncertain (see http://www.businessballs.com/treeswing.htm).\nBut it certainly depicts some common project management problems :-)"
    }

    ListElement {
        settingsName: "lifecycle-fr"
        source: "../graphics/backs/lifecycle-fr.png"
        description: "project lifecycle (fr)"
        family: "humor"
        license: "Unknown"
        author: "Unknown"
        about: "Project lifcycle: euphoria, concern, panics, seek the culprits, punish the innocents, reward those that were not involved..."
    }

    ListElement {
        settingsName: "brave-gnu-world"
        source: "../graphics/backs/Brave_GNU_world.png"
        description: "brave GNU world"
        family: "freesoftware"
        license: "CC-BY-SA-2.5"
        author: "David Batley"
        about: "Richard Stallman launched the GNU project in 1983 and is the father of free software. He created the FSF in 1985. He is also the author of Emacs and many other software"
    }

    ListElement {
        settingsName: "stallman"
        source: "../graphics/backs/stallman.png"
        description: "Stallman drawing"
        family: "freesoftware"
        license: "CC-BY-SA"
        author: "Lucy Watts"
        about: "Richard Stallman launched the GNU project in 1983 and is the father of free software. He created the FSF in 1985. He is also the author of Emacs and many other software"
    }

    ListElement {
        settingsName: "communism"
        source: "../graphics/backs/communism.png"
        description: "communism..."
        family: "freesoftware"
        license: "Unknown"
        author: "Unknown"
        about: "In year 2000, Steve Ballmer of Microsoft, compared Linux to communism. It seems it was an insult in his mouth :-)"
    }

    ListElement {
        settingsName: "eiffel"
        source: "../graphics/backs/eiffel.png"
        description: "eiffel tower"
        family: "engeniering"
        license: "Unknown"
        author: "Unknown"
        about: "The eiffel tower was a prototype, the first tower of more than 300 meters. It was built on budget and on time..."
    }

    ListElement {
        settingsName: "eiffel_plan"
        source: "../graphics/backs/Maurice_koechlin_pylone.png"
        description: "eiffel tower plan"
        family: "engeniering"
        license: "Public Domain (1884)"
        author: "Maurice Koechlin, Émile Nouguier"
        about: "The eiffel tower was a prototype, the first tower of more than 300 meters. It was built on budget and on time..."
    }

    function description(index) {
        return i18n.tr(backsLibrary.get(index).description);
    }

    function getCardBackFromName(name) {
        if (name === "random") {
            return getRandomCardBack()
        }
        else {
            for (var index = 1; index < backsLibrary.count; ++index) {
                if (backsLibrary.get(index).settingsName === name) {
                    return backsLibrary.get(index);
                }
            }
        }
    }

    function getRandomCardBack() {
        var rnd = Math.random()
        var step = 1/(backsLibrary.count-1)
        var idx = Math.floor(rnd / step)+1
        return backsLibrary.get(idx)
    }

    function getBackIndex(name) {
        for (var index = 0; index < backsLibrary.count; ++index) {
            if (backsLibrary.get(index).settingsName === name) {
                return index;
            }
        }

    }

    function stringToTranslate() {
        i18n.tr("Tarot cards have been used to predict future for ages...");
        i18n.tr("The tree swing cartoon has been around for several decades, and its origin remains uncertain (see http://www.businessballs.com/treeswing.htm).\nBut it certainly depicts some common project management problems :-)");
        i18n.tr("Project lifcycle: euphoria, concern, panics, seek the culprits, punish the innocents, reward those that were not involved...");
        i18n.tr("Richard Stallman launched the GNU project in 1983 and is the father of free software. He created the FSF in 1985. He is also the author of Emacs and many other software");
        i18n.tr("In year 2000, Steve Ballmer of Microsoft, compared Linux to communism. It seems it was an insult in his mouth :-)");
        i18n.tr("The eiffel tower was a prototype, the first tower of more than 300 meters. It was built on budget and on time...");
    }
}
