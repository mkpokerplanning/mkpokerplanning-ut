import QtQuick 2.0
import Ubuntu.Components 1.2
import "ui"

/*!
    \brief MainView with Tabs element.
*/

MainView {
    // objectName for functional testing purposes (autopilot-qt5)
    objectName: "mkpokerplanning"

    // Note! applicationName needs to match the "name" field of the click manifest
    //applicationName: "com.ubuntu.developer.alci.mkpokerplanning"
    applicationName: "org.mecadu.ut.mkpokerplanning"

    /*
     This property enables the application to change orientation
     when the device is rotated. The default is false.
    */
    //automaticOrientation: true

    // Removes the old toolbar and enables new features of the new header.
    //useDeprecatedToolbar: false

    width: units.gu(60)
    height: units.gu(100)

    backgroundColor: UbuntuColors.darkGrey

    PageStack {
            id: pageStack
            Component.onCompleted: push(Qt.resolvedUrl("ui/MainPage.qml"))
    }

}

